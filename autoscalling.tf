/*
variable "ami" { description = "Regiao US-EAST-1" default = "ami-6869aa05" }
variable "instance_type" { description = "Regiao US-EAST-1" default = "t2.micro" }

resource "aws_launch_configuration" "agent-lc" {
  name_prefix = "agent-lc-"
  image_id = "${var.ami}"
  instance_type = "${var.instance_type}"
  user_data = "${file("init.sh")}"
  security_groups = ["${aws_security_group.web.id}"]

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "10"
  }
}

resource "aws_autoscaling_group" "agents" {
  availability_zones = ["us-east-1a"]
  name = "agents"
  max_size = "3"
  min_size = "1"
  health_check_grace_period = 300
  health_check_type = "EC2"
  desired_capacity = 2
  force_delete = true
  launch_configuration = "${aws_launch_configuration.agent-lc.name}"

  tag {
    key = "Name"
    value = "Agent Instance"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "agents-scale-up" {
  name = "agents-scale-up"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.agents.name}"
}

resource "aws_autoscaling_policy" "agents-scale-down" {
  name = "agents-scale-down"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "${aws_autoscaling_group.agents.name}"
}
/*
output "agents-scale-up-arn" {
  value = "${aws_autoscaling_policy.agents-scale-up.arn}"
}

output "agents-scale-down-arn" {
  value = "${aws_autoscaling_policy.agents-scale-down.arn}"
}

output "agents-name" {
  value = "${aws_autoscaling_group.agents.name}"
}*/