variable "public_subnet_cidr" {
  description = "CIDR Public Subnet"
  default = "10.0.1.0/24"
}

#Public Subnet
resource "aws_subnet" "us_east_1a_public" {
  vpc_id = "${aws_vpc.vpc_a.id}"

  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "us-east-1a"

  tags {
    Name = "Public Subnet"
  }
}

output "public_subnet" {
  value = "${aws_subnet.us_east_1a_public.id}"
}