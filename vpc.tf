variable "vpc_a_cidr" {
  description = "CIDR VPC A"
  default = "10.0.0.0/16"
}

resource "aws_vpc" "vpc_a" {
  cidr_block = "${var.vpc_a_cidr}"
  instance_tenancy = "default"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
  tags {
    Name = "VPC A"
  }
}

output "vpc_a_id" {
  value = "${aws_vpc.vpc_a.id}"
}

