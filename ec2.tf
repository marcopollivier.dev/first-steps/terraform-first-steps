resource "aws_instance" "main_instance" {
  ami = "ami-6869aa05" # us-west-2
  instance_type = "t2.nano"
  associate_public_ip_address = true #tag que eu procurei para dar acesso publico
  user_data = "${file("init.sh")}"
  security_groups = ["${aws_security_group.web.id}"]
  subnet_id = "${aws_subnet.us_east_1a_public.id}"
}