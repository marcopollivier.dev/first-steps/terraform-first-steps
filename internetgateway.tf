#Internet gateway
resource "aws_internet_gateway" "vpc_a_igw" {
  vpc_id = "${aws_vpc.vpc_a.id}"
}


#route table public subnet
resource "aws_route_table" "us_east_1a_public" {
  vpc_id = "${aws_vpc.vpc_a.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.vpc_a_igw.id}"
  }

  tags {
    Name = "Public Subnet"
  }
}

#associa route table a Gateway de internet
resource "aws_route_table_association" "us_east_1a_public" {
  subnet_id = "${aws_subnet.us_east_1a_public.id}"
  route_table_id = "${aws_route_table.us_east_1a_public.id}"
}

output "vpc_a_igw" {value = "${aws_internet_gateway.vpc_a_igw.id}"}